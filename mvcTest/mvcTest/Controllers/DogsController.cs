﻿using fb_mvc_test_app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace mvcTest.Controllers
{
    public class DogsController : Controller
    {
        // GET: Dogs
        public ActionResult Index(string ToyList, string dogName)
        {

            var allToysList = new List<string>();

            var dogsToShow = from d in Dogs.MyDos
                             select d;

            var toys = from t in Dogs.MyDos
                       orderby t.Toy
                       select t.Toy;

            var toysList = toys.Distinct();
            allToysList.AddRange(toysList.Distinct());
            ViewBag.ToyList = new SelectList(allToysList);


            if (!String.IsNullOrEmpty(dogName))
            {
                dogsToShow = dogsToShow.Where(d => d.Name.Contains(dogName));
            }

            if (!String.IsNullOrEmpty(ToyList))
            {
                dogsToShow = dogsToShow.Where(d => d.Toy.Contains(ToyList));
            }

            //return View(db.Dogs.ToList());
            return View(dogsToShow.ToList());
        }

        // GET: Dogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Dogs dogs = db.Dogs.Find(id);
            Dogs dogs = Dogs.MyDos.Find(p => p.ID == id);
            if (dogs == null)
            {
                return HttpNotFound();
            }
            return View(dogs);
        }

        // GET: Dogs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Toy,Color")] Dogs dogs)
        {
            if (ModelState.IsValid)
            {
                //db.Dogs.Add(dogs);
                //db.SaveChanges();
                Dogs.MyDos.Add(dogs);
                return RedirectToAction("Index");
            }

            return View(dogs);
        }

        // GET: Dogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Dogs dogs = db.Dogs.Find(id);
            Dogs dogs = Dogs.MyDos.Find(p => p.ID == id);
            if (dogs == null)
            {
                return HttpNotFound();
            }
            return View(dogs);
        }

        // POST: Dogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Toy,Color")] Dogs dogs)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(dogs).State = EntityState.Modified;
                //db.SaveChanges();
                var oldDog = Dogs.MyDos.Find(p => p.ID == dogs.ID);
                Dogs.MyDos.Remove(oldDog);
                Dogs.MyDos.Add(dogs);
                return RedirectToAction("Index");
            }
            return View(dogs);
        }

        // GET: Dogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Dogs dogs = db.Dogs.Find(id);
            Dogs dogs = Dogs.MyDos.Find(p => p.ID == id);
            if (dogs == null)
            {
                return HttpNotFound();
            }
            return View(dogs);
        }

        // POST: Dogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Dogs dogs = db.Dogs.Find(id);
            Dogs dogs = Dogs.MyDos.Find(p => p.ID == id);
            //db.Dogs.Remove(dogs);
            Dogs.MyDos.Remove(dogs);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
