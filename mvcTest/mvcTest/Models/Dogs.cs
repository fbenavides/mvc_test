﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace fb_mvc_test_app.Models
{
    public class Dogs
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Toy { get; set; }
        public string Color { get; set; }

        public static List<Dogs> MyDos = new List<Dogs>
        {
            new Dogs{ID=1, Name="Oreo", Toy="Ball", Color="Grey"},
            new Dogs{ID=2, Name="Pepe", Toy="IDK", Color="White"},
            new Dogs{ID=3, Name="Aramis", Toy="Toy3", Color="Black"},
            new Dogs{ID=4, Name="Sofia", Toy="Toy4", Color="Pink"},
        };
    }
}